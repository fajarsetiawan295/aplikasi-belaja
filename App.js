import React, {Component} from 'react';
import {View, Text, StatusBar} from 'react-native';
import {Provider} from 'react-redux';
import {ReduxNetworkProvider} from 'react-native-offline';
import DropdownAlert from 'react-native-dropdownalert';
import {PersistGate} from 'redux-persist/integration/react';
import MainNavigator from './app/services/app.navigation';
import {store, persistor} from './app/redux/store';

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <ReduxNetworkProvider>
          <PersistGate persistor={persistor}>
            <MainNavigator />
          </PersistGate>
        </ReduxNetworkProvider>
      </Provider>
    );
  }
}

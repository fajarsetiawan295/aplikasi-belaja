import React, {Component} from 'react';
import {View, Text, StyleSheet, ActivityIndicator} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {colors} from './../conf';
import LottieView from 'lottie-react-native';

class Loading extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  changeValueInt(value) {
    return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  }

  render() {
    return (
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <View
          style={{
            backgroundColor: 'transparent',
            borderRadius: 10,
            width: wp('50'),
            height: wp('50'),
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <LottieView
            source={require('../assets/lottie/loading.json')}
            autoPlay
            loop
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  activityIndicator: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 80,
  },
});

export default Loading;

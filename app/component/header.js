import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Icon from './icons';
import colors from '../conf/color.global';

class header extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    const {navigation} = this.props;
    return (
      <View style={styles.headerContainer}>
        <View style={styles.subContainerHeader}>
          <TouchableOpacity onPress={this.props.onPress}>
            <Icon.FontAwesome
              name="arrow-left"
              color={colors.colorsLight}
              size={20}
            />
          </TouchableOpacity>
          <Text style={styles.textHeader}>{this.props.title}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: '#FAFAFA',
    flex: 1,
  },
  headerContainer: {
    width: wp('100%'),
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomWidth: 1,
    backgroundColor: 'white',
    borderColor: '#70707026',
    height: hp('9%'),
  },
  subContainerHeader: {
    flexDirection: 'row',
    width: wp('90%'),
    // justifyContent: 'space-between'
  },
  textHeader: {
    marginLeft: wp('5%'),
    color: colors.colorsLight,
    fontSize: wp('5%'),
    fontFamily: 'ProximaNova',
  },
});

export default header;

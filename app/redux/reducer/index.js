import { combineReducers } from 'redux';
import AuthReducer from './authReducer';
import { reducer as network } from 'react-native-offline';

const rootReducers = combineReducers({
  AuthReducer: AuthReducer,
  network, // Network reducers
});

export default rootReducers;

// export const url = 'http://192.168.1.8:8000';
export const url = 'https://pembelajaraniqro.herokuapp.com';
export const alquran = 'http://api.alquran.cloud/v1';
export const jadwalsolat = 'https://api.banghasan.com/sholat';

export const url_api = url + '/api';

export const api = {
  Login: url_api + '/auth/login',
  Register: url_api + '/auth/signup',
  Profile: url_api + '/auth/Profile',

  cetakuserpdf: url_api + '/cetak_pdf_user/',
  cetakusernilaipdf: url_api + '/cetak_pdf/',

  //
  ListSurah: alquran + '/surah',

  // solat
  FormatKota: jadwalsolat + '/format/json/kota',
  JadwalSolat: jadwalsolat + '/format/json/jadwal/kota/',
};

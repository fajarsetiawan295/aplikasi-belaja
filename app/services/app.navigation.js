import * as React from 'react';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {createMaterialBottomTabNavigator} from 'react-navigation-material-bottom-tabs';
import createAnimatedSwitchNavigator from 'react-navigation-animated-switch';
import {Transition} from 'react-native-reanimated';
import Icon from './../component/icons';
import {login, register, splashScreen} from '../screen/auth';
import * as homeItem from './../screen/home';
import * as tabbottom from './../screen/tab.bottom';
import * as Pro from './../screen/Profile';
import * as Surah from './../screen/surat';
import * as Solat from './../screen/Solat';
import * as Bel from './../screen/Belajar';
import * as kus from './../screen/Kuis';

import demo from './../screen/demo';

const TabBottom = createMaterialBottomTabNavigator(
  {
    Home: {
      screen: tabbottom.home,
      navigationOptions: {
        tabBarLabel: 'Home',
        tabBarIcon: ({tintColor}) => (
          <Icon.MaterialIcons name="home" color={tintColor} size={25} />
        ),
      },
    },
    // History: {
    //   screen: tabbottom.history,
    //   navigationOptions: {
    //     tabBarLabel: 'History',
    //     tabBarIcon: ({tintColor}) => (
    //       <Icon.MaterialIcons name="assignment" color={tintColor} size={25} />
    //     ),
    //   },
    // },
    Profile: {
      screen: tabbottom.profile,
      navigationOptions: {
        tabBarLabel: 'Profile',
        tabBarIcon: ({tintColor}) => (
          <Icon.MaterialIcons
            name="account-circle"
            color={tintColor}
            size={25}
          />
        ),
      },
    },
  },
  {
    initialRouteName: 'Home',
    activeColor: '#B08158',
    inactiveColor: 'black',
    barStyle: {
      activeColor: '#B08158',
      backgroundColor: 'white',
    },
  },
);

const Auth = createStackNavigator(
  {
    Login: login,
    Register: register,
  },
  {
    initialRouteName: 'Login',
    headerMode: 'none',
  },
);

const HomeNavigator = createStackNavigator(
  {
    Demo: demo,
    Home: tabbottom.home,
    History: tabbottom.history,
    Profile: tabbottom.profile,

    ListSurat: Surah.ListSurat,
    IsiSurat: Surah.IsiSurat,

    ListKota: Solat.ListKota,
    JadwalSolat: Solat.JadwalSolat,

    ListBelajar: Bel.ListBelajar,
    ListHuruf: Bel.ListHuruf,
    ListBenda: Bel.ListBenda,

    Kuis: kus.Kuis,

    TabBottom: TabBottom,
  },
  {
    initialRouteName: 'TabBottom',
    headerMode: 'none',
  },
);

const MainNavigator = createAnimatedSwitchNavigator(
  {
    SplashScreen: splashScreen,
    Auth: Auth,
    HomeNavigator: HomeNavigator,
  },
  {
    transition: (
      <Transition.Together>
        <Transition.Out
          type="slide-right"
          durationMs={400}
          interpolation="easeIn"
        />
        <Transition.In type="slide-left" durationMs={400} />
      </Transition.Together>
    ),
  },
);

export default createAppContainer(MainNavigator);

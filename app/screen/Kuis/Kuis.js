import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  ScrollView,
  TouchableOpacity,
  Linking,
} from 'react-native';
import {Icon, Header} from './../../component';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {colors} from '../../conf';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {dataUser, isLogged, tokenUser} from './../../redux/actions';
import {api} from './../../conf/url';
import {Get_services} from './../../services';
import LottieView from 'lottie-react-native';
import DropdownAlert from 'react-native-dropdownalert';
import SoundPlayer from 'react-native-sound-player';
import Sound from 'react-native-sound';
import moment from 'moment';

class ListBenda extends Component {
  constructor(props) {
    super(props);
    this.state = {
      surah: 0,
      nilai: 0,
      m: 0,
      statusrt: true,
      mes: '',
      alif: [
        {
          id: '1',
          sound: 'buku.mp3',
          a: 'Bantal',
          b: 'Atap',
          c: 'Buku',
          d: 'guling',
          jawaban: 'Buku',
        },
        {
          id: '2',
          sound: 'bantal.mp3',
          a: 'Bantal',
          b: 'Atap',
          c: 'Buku',
          d: 'papan tulis',
          jawaban: 'bantal',
        },
        {
          id: 3,
          sound: 'papantulis.mp3',
          a: 'Bantal',
          b: 'Atap',
          c: 'Buku',
          d: 'papan tulis',
          jawaban: 'Papan Tulis',
        },
        {
          id: 4,
          sound: 'cermin.mp3',
          a: 'Bantal',
          b: 'Atap',
          c: 'cermin',
          d: 'papan tulis',
          jawaban: 'cermin',
        },
        {
          id: 5,
          sound: 'ba.mp3',
          a: 'alif',
          b: 'ba',
          c: 'jim',
          d: 'ha',
          jawaban: 'ba',
        },
        {
          id: 6,
          sound: 'ta.mp3',
          a: 'alif',
          b: 'ta',
          c: 'jim',
          d: 'ha',
          jawaban: 'ta',
        },
        {
          id: 7,
          sound: 'ya.mp3',
          a: 'alif',
          b: 'ta',
          c: 'jim',
          d: 'ha',
          jawaban: 'ya',
        },
        {
          id: 8,
          sound: 'tujuhh.mp3',
          a: '1',
          b: '7',
          c: '5',
          d: '4',
          jawaban: '7',
        },
        {
          id: 9,
          sound: 'delapann.mp3',
          a: '1',
          b: '7',
          c: '5',
          d: '8',
          jawaban: '8',
        },
        {
          id: 10,
          sound: 'sembilaan.mp3',
          a: '9',
          b: '7',
          c: '5',
          d: '8',
          jawaban: '9',
        },
        {
          id: 11,
          sound: 'sepuluhh.mp3',
          a: '1',
          b: '10',
          c: '5',
          d: '8',
          jawaban: '10',
        },
      ],
    };
  }

  playSong(x) {
    let sound = new Sound(x, Sound.MAIN_BUNDLE, (error) => {
      if (error) {
        console.log('failed to load the sound', error);
      } else {
        sound.play();
      }
    });

    console.log(x);
  }

  random() {
    this.setState({surah: parseInt(Math.random() * 10)});
    console.log(this.state.surah);
  }
  UNSAFE_componentWillMount() {
    this.random();
  }

  hitung(x, t) {
    console.log(x, t);
    this.state.alif.map((item, key) => {
      if (x == item.id) {
        if (item.jawaban == t) {
          this.con();
        }
      }
    });
    this.setState({m: this.state.m + 1});
    this.dav();
    this.random();
  }
  con() {
    this.setState({nilai: this.state.nilai + 1});
  }
  dav() {
    if (this.state.m == 10) {
      this.setState({statusrt: false});
    }
  }
  linkdownload() {
    Linking.openURL(
      api.cetakusernilaipdf +
        this.state.nilai +
        '/' +
        this.props.infoUser.name +
        '/' +
        this.props.infoUser.email +
        '/' +
        this.props.infoUser.kelas +
        '/' +
        this.props.infoUser.sekolah,
    );
  }

  render() {
    const {navigation} = this.props;
    const List = ({suara, a, b, c, d, id}) => {
      return (
        <>
          <View style={styles.containerAddress}>
            <TouchableOpacity
              style={{
                marginVertical: hp('1'),
                width: wp('90'),
                backgroundColor: 'white',
                borderRadius: 10,
                alignItems: 'center',
                justifyContent: 'center',
              }}
              onPress={() => this.playSong(suara)}>
              <View style={{marginTop: wp('3%')}}></View>

              <Icon.AntDesign
                name={'caretright'}
                color={colors.colorssatu}
                size={wp('6%')}
              />
            </TouchableOpacity>

            <View style={{marginTop: wp('3%'), padding: wp('4%')}}>
              <Text>
                Coba Tebak Suara Di atas Jika Kedalam Bahasa Indonesia Menjadi
                Apa
              </Text>
            </View>
            <TouchableOpacity
              style={styles.subContainerAddress}
              onPress={() => this.hitung(id, a)}>
              <Text style={{fontSize: wp('5')}}>A. {a}</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.subContainerAddress}
              onPress={() => this.hitung(id, b)}>
              <Text style={{fontSize: wp('5')}}>B. {b}</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.subContainerAddress}
              onPress={() => this.hitung(id, c)}>
              <Text style={{fontSize: wp('5')}}>C. {c}</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.subContainerAddress}
              onPress={() => this.hitung(id, d)}>
              <Text style={{fontSize: wp('5'), marginBottom: hp('3%')}}>
                D. {d}
              </Text>
            </TouchableOpacity>
          </View>
        </>
      );
    };

    return (
      <View style={styles.container}>
        <Header
          onPress={() => navigation.goBack()}
          title={'AYO KITA BELAJAR BENDA'}
        />
        {this.state.statusrt == false ? (
          <View style={styles.containerAddress}>
            <View
              style={{
                marginTop: wp('3%'),
                padding: wp('4%'),
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text>Hore Kamu Dapat Nilai Seperti Berikut</Text>
              <Text style={{fontSize: 20, marginTop: hp('5%')}}>
                {this.state.nilai}
              </Text>
            </View>
            <TouchableOpacity
              onPress={() => this.linkdownload()}
              style={styles.button}>
              <Text style={styles.textButton}>Print</Text>
            </TouchableOpacity>
          </View>
        ) : (
          <ScrollView showsVerticalScrollIndicator={false}>
            <List
              suara={this.state.alif[this.state.surah].sound}
              imgd={this.state.alif[this.state.surah].img}
              a={this.state.alif[this.state.surah].a}
              b={this.state.alif[this.state.surah].b}
              c={this.state.alif[this.state.surah].c}
              d={this.state.alif[this.state.surah].d}
              id={this.state.alif[this.state.surah].id}
            />
          </ScrollView>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#E9EBE4',
  },
  containerProfile: {
    marginTop: hp('5'),
    width: wp('90'),
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
  },
  profileRight: {
    width: wp('55'),
  },
  imageProfile: {
    width: wp('25'),
    height: wp('25'),
    borderRadius: wp('20'),
    borderWidth: 1,
    borderColor: 'white',
  },
  textNameProfile: {
    // fontFamily: Fonts.type.emphasis,
    // fontSize: Fonts.size.regular,
  },
  textEmailProfile: {
    // fontSize: Fonts.size.small,
    // fontFamily: Fonts.type.emphasis,
    color: '#515C6F',
  },
  button: {
    width: wp('80%'),
    marginTop: hp('4%'),
    borderRadius: 7,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.colorstiga,
    height: hp('6%'),
    marginBottom: hp('6%'),
  },
  textButton: {
    fontFamily: 'ProximaNova',
    color: colors.colorsdua,
    fontSize: wp('5%'),
  },
  buttonEditProfile: {
    marginTop: hp('2'),
    borderRadius: 14,
    width: wp('50'),
    borderColor: '#727C8E4D',
    borderWidth: 1,
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textButtonEditProfile: {
    // fontFamily: Fonts.type.bold,
    // fontSize: Fonts.size.medium,
    color: '#727C8E',
    marginVertical: wp('1'),
  },
  containerAddress: {
    marginVertical: hp('1'),
    width: wp('90'),
    backgroundColor: 'white',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: hp('20%'),
  },
  subContainerAddress: {
    marginVertical: hp('1'),
    justifyContent: 'space-evenly',
    width: wp('80'),
    alignItems: 'center',
    flexDirection: 'row',
  },
  textList: {
    width: wp('50'),
    color: '#515C6F',
  },
  imgcard: {
    width: wp('70%'),
    height: wp('70%'),
    borderTopLeftRadius: wp('3%'),
    borderTopRightRadius: wp('3%'),
    borderWidth: 1,
  },
});

const mapStateToProps = ({AuthReducer, network}) => {
  const {token, infoUser} = AuthReducer;
  const {isConnected} = network;
  return {isConnected, token, infoUser};
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({dataUser, isLogged, tokenUser}, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(ListBenda);

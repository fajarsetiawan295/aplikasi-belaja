import React, {Component} from 'react';
import {
  Alert,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  ScrollView,
  StyleSheet,
  Image,
  CheckBox,
  ActivityIndicator,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {colors} from './../../conf';
import {post_services} from './../../services';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {isLogged, tokenUser} from '../../redux/actions';
import DropdownAlert from 'react-native-dropdownalert';
import {api} from './../../conf/url';
import {Loading} from './../../component';
import SwitchSelector from 'react-native-switch-selector';

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      name: '',
      passwd: '',
      kelas: '',
      sekolah: '',
      password_confirmation: '',
      lifetime: false,
      isLoading: false,
      animating: false,
    };
  }

  onLoginPress() {
    this.setState({animating: true});
    const state = this.state;
    const {navigation} = this.props;

    const data = new FormData();
    data.append('name', state.name);
    data.append('email', state.email);
    data.append('password', state.passwd);
    data.append('password_confirmation', state.password_confirmation);
    data.append('kelas', state.kelas);
    data.append('sekolah', state.sekolah);

    post_services(null, data, api.Register).then((rspons) => {
      this.setState({animating: false});
      if (rspons.status == 200) {
        navigation.navigate('Login');
      } else {
        this.dropDownAlertRef.alertWithType(
          'error',
          'Error',
          rspons.data.message,
        );
      }
    });
  }

  render() {
    const {navigation} = this.props;
    const state = this.state;

    return (
      <View style={styles.container}>
        {this.state.animating == true ? (
          <View>
            <Loading />
          </View>
        ) : (
          <View style={styles.container}>
            <ScrollView
              contentContainerStyle={{alignItems: 'center'}}
              showsVerticalScrollIndicator={false}>
              <View style={styles.cardFormInputemail}>
                <Text style={styles.textFormTitle}>Name</Text>
                <TextInput
                  value={state.name}
                  style={styles.formText}
                  onChangeText={(x) => this.setState({name: x})}
                />
              </View>
              <View style={styles.cardFormInput}>
                <Text style={styles.textFormTitle}>Email</Text>
                <TextInput
                  value={state.email}
                  style={styles.formText}
                  onChangeText={(x) => this.setState({email: x})}
                />
              </View>
              <View style={styles.cardFormInput}>
                <Text style={styles.textFormTitle}>Kelas</Text>
                <TextInput
                  value={state.kelas}
                  style={styles.formText}
                  onChangeText={(x) => this.setState({kelas: x})}
                />
              </View>
              <View style={styles.cardFormInput}>
                <Text style={styles.textFormTitle}>Nama Sekolah</Text>
                <TextInput
                  value={state.sekolah}
                  style={styles.formText}
                  onChangeText={(x) => this.setState({sekolah: x})}
                />
              </View>
              <View style={styles.cardFormInput}>
                <Text
                  style={{
                    color: colors.colorsdua,
                    fontFamily: 'ProximaNova',
                    fontSize: wp('4.5%'),
                  }}>
                  Password
                </Text>
                <TextInput
                  value={state.passwd}
                  style={styles.formText}
                  secureTextEntry={true}
                  onChangeText={(x) => this.setState({passwd: x})}
                />
              </View>
              <View style={styles.cardFormInput}>
                <Text
                  style={{
                    color: colors.colorsdua,
                    fontFamily: 'ProximaNova',
                    fontSize: wp('4.5%'),
                  }}>
                  Password Confirmation
                </Text>
                <TextInput
                  value={state.password_confirmation}
                  style={styles.formText}
                  secureTextEntry={true}
                  onChangeText={(x) =>
                    this.setState({password_confirmation: x})
                  }
                />
              </View>

              <TouchableOpacity
                onPress={() => this.onLoginPress()}
                style={styles.button}>
                {state.isLoading == true ? (
                  <ActivityIndicator color={'red'} size="small" />
                ) : (
                  <Text style={styles.textButton}>Register</Text>
                )}
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => navigation.navigate('Login')}
                style={styles.buttonregister}>
                <Text style={{color: colors.colorsdua}}>Login</Text>
              </TouchableOpacity>
            </ScrollView>
          </View>
        )}
        <DropdownAlert
          ref={(ref) => (this.dropDownAlertRef = ref)}
          closeInterval={5000}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textFormTitle: {
    color: colors.colorsdua,
    fontFamily: 'ProximaNova',
    fontSize: wp('4.5%'),
  },
  cardFormInput: {
    width: wp('80%'),
    marginTop: hp('2%'),
  },
  cardFormInputemail: {
    width: wp('80%'),
    marginTop: hp('10%'),
  },
  formText: {
    height: hp('6'),
    width: wp('80%'),
    paddingLeft: 10,
    borderRadius: 7,
    borderWidth: 1,
    borderColor: colors.colorstiga,
    color: colors.colorsdua,
    fontFamily: 'ProximaNova',
    // elevation: 1,
    // opacity: 0.99
  },
  button: {
    width: wp('80%'),
    marginTop: hp('4%'),
    borderRadius: 7,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.colorstiga,
    height: hp('6%'),
  },
  buttonregister: {
    width: wp('80%'),
    marginTop: hp('4%'),
    marginBottom: hp('5%'),
    borderRadius: 7,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: colors.colorstiga,
    height: hp('6%'),
  },
  checkboxContent: {
    flexDirection: 'row',
    width: wp('80'),
    alignItems: 'center',
    marginTop: hp('1'),
  },
  textButton: {
    fontFamily: 'ProximaNova',
    color: colors.colorsdua,
    fontSize: wp('5%'),
  },
  textQuestionAccount: {
    fontFamily: 'ProximaNova',
    color: '#626270',
    fontSize: wp('4.5%'),
  },
  imageLogo: {
    width: wp('70%'),
    height: hp('20%'),
    resizeMode: 'cover',
  },
  activityIndicator: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 80,
  },
});

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({tokenUser, isLogged}, dispatch);
};

export default connect(null, mapDispatchToProps)(Register);

import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  YellowBox,
  TouchableOpacity,
  ScrollView,
  RefreshControl,
  Image,
  Linking,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {colors} from '../../conf';
import {FlatGrid} from 'react-native-super-grid';
import Icon from './../../component/icons';
import {Get_services} from './../../services';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {dataUser, isLogged} from './../../redux/actions';
import {api, url} from './../../conf/url';
import {Loading} from './../../component';
import icons from './../../component/icons';

YellowBox.ignoreWarnings([
  'Calling `getNode()` on the ref of an Animated component is no longer necessary. You can now directly use the ref instead.',
  'VirtualizedLists should never be nested',
]);

class home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      headItem: [],
      animating: false,
      informasi: [
        {
          nav: 'ListBelajar',
          t: 'hijahiyah',
          judul: 'Belajar Hijahiyah',
          img: 'https://project295.000webhostapp.com/1.jpeg',
        },
        {
          nav: 'ListHuruf',
          t: 'hijahiyah',
          judul: 'Belajar Huruf',
          img: 'https://project295.000webhostapp.com/ANGKA/1.jpeg',
        },
        {
          nav: 'ListBenda',
          t: 'hijahiyah',
          judul: 'Belajar Benda',
          img: 'https://project295.000webhostapp.com/benda/meja.jpeg',
        },
      ],
      refresh: false,
    };
  }

  onGetProfile() {
    this.setState({animating: true});
    Get_services(this.props.token, api.Profile).then((response) => {
      this.setState({animating: false});
      if (response.status == 200) {
        this.props.dataUser(response.data.data);
        this.funIcon();
      } else if (response.status == 401) {
        this.props.isLogged(false);
        this.props.navigation.navigate('Login');
      } else {
        console.log(response, 'profile');
      }
    });
  }

  UNSAFE_componentWillMount() {
    this.onRequestAll();
  }

  onRequestAll() {
    this.onGetProfile();
  }
  refreshContent() {
    this.setState({refresh: true});
    this.onRequestAll();
    this.setState({refresh: false});
  }

  funIcon() {
    this.setState({
      headItem: [
        {
          item: 'Surat',
          name: 'abjad-arabic',
          navigate: 'ListSurat',
          iconType: Icon.MaterialCommunityIcons,
        },
        {
          item: 'Import',
          name: 'arrow-swap',
          navigate: '',
          iconType: Icon.Fontisto,
        },
        {
          item: 'Kuis',
          name: 'money-bill',
          navigate: 'Kuis',
          iconType: Icon.FontAwesome5,
        },
        {
          item: 'Jadwal Solat',
          name: 'qrcode',
          navigate: 'ListKota',
          iconType: Icon.FontAwesome,
        },
      ],
    });

    this.setState({animating: false});
  }

  pressGridIcon(x) {
    const {navigation} = this.props;
    if (x != '') {
      navigation.navigate(x);
    } else {
      Linking.openURL(api.cetakuserpdf + this.props.infoUser.id);
    }
  }

  render() {
    const {navigation} = this.props;
    const img = require('./../../assets/image/test.png');
    const List = ({onPress, judul, imgd, t}) => {
      return (
        <View style={styles.containercard}>
          <View style={styles.subcontainercard}>
            <ScrollView
              showsVerticalScrollIndicator={false}
              horizontal={true}
              showsHorizontalScrollIndicator={false}>
              <TouchableOpacity style={styles.card} onPress={onPress}>
                <Image
                  source={{
                    uri: imgd,
                  }}
                  style={styles.imgcard}
                />
                <Text style={styles.judulcard}>{judul}</Text>
              </TouchableOpacity>
            </ScrollView>
          </View>
        </View>
      );
    };
    return (
      // header
      <View style={styles.container}>
        {this.state.animating == true ? (
          <View>
            <Loading />
          </View>
        ) : (
          <View style={styles.container}>
            <View
              style={{
                height: hp('15%'),
                width: wp('100%'),
                borderBottomRightRadius: hp('25%'),
                backgroundColor: colors.colorstiga,
                flexDirection: 'row',
              }}>
              <Image
                source={{
                  uri:
                    'https://png.pngtree.com/element_our/png_detail/20181102/avatar-profile-logo-vector-emblem-illustration-modern-illustration-png_227486.jpg',
                }}
                style={styles.imageProfile}
              />
              <View>
                <Text
                  style={{
                    marginLeft: wp('2%'),
                    marginTop: hp('3%'),
                    fontSize: wp('5%'),
                    fontWeight: 'bold',
                  }}>
                  {this.props.infoUser.name}
                </Text>
                <Text
                  style={{
                    marginLeft: wp('2%'),
                    // marginTop: hp('1%'),
                    // fontWeight: 'bold',
                    fontSize: wp('4%'),
                  }}>
                  {this.props.infoUser.sekolah}
                </Text>
              </View>
            </View>

            <View style={styles.containerIconGrid}>
              <View style={styles.subContainerIconGrid}>
                {this.state.headItem.map((item, key) => {
                  return (
                    <TouchableOpacity
                      onPress={() => this.pressGridIcon(item.navigate)}
                      style={styles.componentGridIcon}
                      key={key}>
                      <View style={styles.subComponentGridCyrcle}>
                        <item.iconType
                          name={item.name}
                          color={colors.colorssatu}
                          size={wp('6%')}
                        />
                      </View>
                      <Text
                        numberOfLines={1}
                        ellipsizeMode="tail"
                        style={styles.subComponentGridText}>
                        {item.item}
                      </Text>
                    </TouchableOpacity>
                  );
                })}
              </View>
            </View>
            <ScrollView
              refreshControl={
                <RefreshControl
                  refreshing={this.state.refresh}
                  onRefresh={() => this.refreshContent()}
                />
              }
              showsVerticalScrollIndicator={false}>
              {this.state.informasi.map((item, key) => {
                return (
                  <List
                    judul={item.judul}
                    t={item.t}
                    imgd={item.img}
                    onPress={() => this.props.navigation.navigate(item.nav)}
                  />
                );
              })}
            </ScrollView>
          </View>
        )}
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.YellowButton,
    alignItems: 'center',
  },
  imageProfile: {
    marginTop: hp('1%'),
    marginLeft: wp('2%'),
    width: wp('20'),
    height: wp('20'),
    borderRadius: wp('20'),
    borderWidth: 1,
  },
  containerIconGrid: {
    width: wp('95%'),
    backgroundColor: colors.colorstiga,
    borderRadius: 10,
    borderTopLeftRadius: wp('10%'),
    borderTopRightRadius: wp('10%'),
    alignItems: 'center',
    marginTop: hp('2%'),
  },
  subContainerIconGrid: {
    margin: wp('2%'),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: wp('90%'),
  },
  componentGridIcon: {
    alignItems: 'center',
    justifyContent: 'center',
    width: wp('20%'),
  },
  subComponentGridCyrcle: {
    borderRadius: wp('20%'),
    backgroundColor: 'white',
    height: wp('15%'),
    width: wp('15%'),
    alignItems: 'center',
    justifyContent: 'center',
  },
  subComponentGridText: {
    marginTop: 1,
    fontFamily: 'ProximaNova',
    fontSize: wp('3%'),
    color: colors.colorssatu,
    width: wp('22%'),
    alignItems: 'center',
    textAlign: 'center',
  },
  containercard: {
    width: wp('90%'),
    marginTop: hp('1%'),
  },
  textcategory: {
    fontSize: wp('5%'),
    fontWeight: 'bold',
  },
  subcontainercard: {
    width: wp('90%'),
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  card: {
    marginLeft: wp('2%'),
    marginTop: hp('2%'),
    width: wp('87%'),
    height: hp('35%'),
    borderRadius: wp('3%'),
    borderColor: colors.colorstiga,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    backgroundColor: 'white',
  },
  imgcard: {
    width: wp('45%'),
    height: wp('40%'),
    borderTopLeftRadius: wp('3%'),
    borderTopRightRadius: wp('3%'),
    borderWidth: 1,
  },
  judulcard: {
    marginLeft: wp('2%'),
    marginTop: hp('1%'),
    fontWeight: 'bold',
  },
  iconcomment: {
    marginTop: hp('3%'),
    width: wp('40%'),
    alignItems: 'flex-end',
  },
});

// export default home;
const mapStateToProps = ({AuthReducer, network}) => {
  const {token, infoUser} = AuthReducer;
  const {isConnected} = network;
  return {
    isConnected,
    token,
    infoUser,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      dataUser,
      isLogged,
    },
    dispatch,
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(home);

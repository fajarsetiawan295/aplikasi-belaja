import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {Icon, Header} from './../../component';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {colors} from '../../conf';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {dataUser, isLogged, tokenUser} from './../../redux/actions';
import {api} from './../../conf/url';
import {Get_services} from './../../services';
import LottieView from 'lottie-react-native';
import DropdownAlert from 'react-native-dropdownalert';
import SoundPlayer from 'react-native-sound-player';
import Sound from 'react-native-sound';

class IsiSurat extends Component {
  constructor(props) {
    super(props);
    this.state = {
      surah: [],
      statusrt: true,
      mes: '',
    };
  }
  onActivesurah() {
    this.setState({statusrt: false});
    const {navigation} = this.props;
    Get_services(
      this.props.token,
      api.ListSurah + '/' + navigation.getParam('id') + '/ar.alafasy',
    ).then((response) => {
      this.setState({statusrt: true});

      if (response.status == 200) {
        this.setState({surah: response.data.data.ayahs});
        console.log('ini user', this.state.surah);
      } else if (response.status == 401) {
        this.props.isLogged(false);
        this.props.navigation.navigate('Login');
      } else {
        this.setState({statusrt: true, mes: response.data.message});
        console.log(response, 'profile');
      }
    });
  }
  playSong(x) {
    let sound = new Sound(x, Sound.MAIN_BUNDLE, (error) => {
      if (error) {
        console.log('failed to load the sound', error);
      } else {
        sound.play();
      }
    });
  }
  UNSAFE_componentWillMount() {
    this.onActivesurah();
  }
  render() {
    const {navigation} = this.props;
    const List = ({onPress, title, ayat, x}) => {
      return (
        <TouchableOpacity
          style={styles.containerAddress}
          onPress={() => this.playSong(x)}>
          <View style={styles.subContainerAddress}>
            <Text>ayat</Text>
            <Text style={[styles.textList, {marginLeft: wp('5%')}]}>
              {ayat}
            </Text>
          </View>
          <View
            style={{
              width: wp('90'),
              alignItems: 'flex-end',
              marginRight: wp('5%'),
              marginBottom: wp('5%'),
            }}>
            <Text style={[styles.textList, {fontSize: 21}]}>{title}</Text>
          </View>
        </TouchableOpacity>
      );
    };

    return (
      <View style={styles.container}>
        <Header onPress={() => navigation.goBack()} title={'List Surah'} />
        <ScrollView showsVerticalScrollIndicator={false}>
          {this.state.statusrt == true ? (
            <>
              {this.state.surah.map((item, key) => {
                return (
                  <>
                    <List
                      title={item.text}
                      ayat={item.number}
                      x={item.audioSecondary[1]}
                    />
                  </>
                );
              })}
            </>
          ) : (
            <View
              style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
              <Text>{this.state.mes}</Text>
              <View
                style={{
                  backgroundColor: 'transparent',
                  borderRadius: 10,
                  width: wp('100'),
                  height: wp('100'),
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <LottieView
                  source={require('../../assets/lottie/empaty.json')}
                  autoPlay
                  loop
                />
              </View>
            </View>
          )}
        </ScrollView>
        <DropdownAlert
          ref={(ref) => (this.dropDownAlertRef = ref)}
          closeInterval={5000}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#E9EBE4',
  },
  containerProfile: {
    marginTop: hp('5'),
    width: wp('90'),
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
  },
  profileRight: {
    width: wp('55'),
  },
  imageProfile: {
    width: wp('25'),
    height: wp('25'),
    borderRadius: wp('20'),
    borderWidth: 1,
    borderColor: 'white',
  },
  textNameProfile: {
    // fontFamily: Fonts.type.emphasis,
    // fontSize: Fonts.size.regular,
  },
  textEmailProfile: {
    // fontSize: Fonts.size.small,
    // fontFamily: Fonts.type.emphasis,
    color: '#515C6F',
  },
  buttonEditProfile: {
    marginTop: hp('2'),
    borderRadius: 14,
    width: wp('50'),
    borderColor: '#727C8E4D',
    borderWidth: 1,
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textButtonEditProfile: {
    // fontFamily: Fonts.type.bold,
    // fontSize: Fonts.size.medium,
    color: '#727C8E',
    marginVertical: wp('1'),
  },
  containerAddress: {
    marginVertical: hp('1'),
    width: wp('90'),
    backgroundColor: 'white',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: wp('5%'),
  },
  subContainerAddress: {
    width: wp('90'),
    padding: wp('5%'),
    flexDirection: 'row',
  },
  textList: {
    width: wp('50'),
    color: '#515C6F',
  },
});

const mapStateToProps = ({AuthReducer, network}) => {
  const {token, infoUser} = AuthReducer;
  const {isConnected} = network;
  return {isConnected, token, infoUser};
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({dataUser, isLogged, tokenUser}, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(IsiSurat);

import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {Icon, Header} from './../../component';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {colors} from '../../conf';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {dataUser, isLogged, tokenUser} from './../../redux/actions';
import {api} from './../../conf/url';
import {Get_services} from './../../services';
import LottieView from 'lottie-react-native';
import DropdownAlert from 'react-native-dropdownalert';
import SoundPlayer from 'react-native-sound-player';
import Sound from 'react-native-sound';
import moment from 'moment';

class ListHuruf extends Component {
  constructor(props) {
    super(props);
    this.state = {
      surah: '',
      statusrt: true,
      mes: '',
      alif: [
        {
          sound: 'satuu.mp3',
          img: 'https://project295.000webhostapp.com/ANGKA/1.jpeg',
        },
        {
          sound: 'duaa.mp3',
          img: 'https://project295.000webhostapp.com/ANGKA/2.jpeg',
        },
        {
          sound: 'tigaa.mp3',
          img: 'https://project295.000webhostapp.com/ANGKA/3.jpeg',
        },
        {
          sound: 'empatt.mp3',
          img: 'https://project295.000webhostapp.com/ANGKA/4.jpeg',
        },
        {
          sound: 'limaa.mp3',
          img: 'https://project295.000webhostapp.com/ANGKA/5.jpeg',
        },
        {
          sound: 'enam.mp3',
          img: 'https://project295.000webhostapp.com/ANGKA/6.jpeg',
        },
        {
          sound: 'tujuhh.mp3',
          img: 'https://project295.000webhostapp.com/ANGKA/7.jpeg',
        },
        {
          sound: 'delapann.mp3',
          img: 'https://project295.000webhostapp.com/ANGKA/8.jpeg',
        },
        {
          sound: 'sembilaan.mp3',
          img: 'https://project295.000webhostapp.com/ANGKA/9.jpeg',
        },
        {
          sound: 'sepuluhh.mp3',
          img: 'https://project295.000webhostapp.com/ANGKA/10.jpeg',
        },
      ],
    };
  }

  playSong(x) {
    let sound = new Sound(x, Sound.MAIN_BUNDLE, (error) => {
      if (error) {
        console.log('failed to load the sound', error);
      } else {
        sound.play();
      }
    });

    console.log(x);
  }

  render() {
    const {navigation} = this.props;
    const List = ({suara, imgd}) => {
      return (
        <TouchableOpacity
          style={styles.containerAddress}
          onPress={() => this.playSong(suara)}>
          <View style={styles.subContainerAddress}>
            <Image
              source={{
                uri: imgd,
              }}
              style={styles.imgcard}
            />
          </View>
        </TouchableOpacity>
      );
    };

    return (
      <View style={styles.container}>
        <Header
          onPress={() => navigation.goBack()}
          title={'AYO KITA BELAJAR HURUF'}
        />
        <ScrollView showsVerticalScrollIndicator={false}>
          {this.state.alif.map((item, key) => {
            return <List suara={item.sound} imgd={item.img} />;
          })}
        </ScrollView>
        <DropdownAlert
          ref={(ref) => (this.dropDownAlertRef = ref)}
          closeInterval={5000}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#E9EBE4',
  },
  containerProfile: {
    marginTop: hp('5'),
    width: wp('90'),
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
  },
  profileRight: {
    width: wp('55'),
  },
  imageProfile: {
    width: wp('25'),
    height: wp('25'),
    borderRadius: wp('20'),
    borderWidth: 1,
    borderColor: 'white',
  },
  textNameProfile: {
    // fontFamily: Fonts.type.emphasis,
    // fontSize: Fonts.size.regular,
  },
  textEmailProfile: {
    // fontSize: Fonts.size.small,
    // fontFamily: Fonts.type.emphasis,
    color: '#515C6F',
  },
  buttonEditProfile: {
    marginTop: hp('2'),
    borderRadius: 14,
    width: wp('50'),
    borderColor: '#727C8E4D',
    borderWidth: 1,
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textButtonEditProfile: {
    // fontFamily: Fonts.type.bold,
    // fontSize: Fonts.size.medium,
    color: '#727C8E',
    marginVertical: wp('1'),
  },
  containerAddress: {
    marginVertical: hp('1'),
    width: wp('90'),
    backgroundColor: 'white',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: wp('5%'),
  },
  subContainerAddress: {
    marginVertical: hp('1'),
    justifyContent: 'space-between',
    width: wp('80'),
    alignItems: 'center',
    // flexDirection: 'row',
  },
  textList: {
    width: wp('50'),
    // fontFamily: Fonts.type.emphasis,
    color: '#515C6F',
  },
  imgcard: {
    width: wp('50%'),
    height: wp('70%'),
    borderTopLeftRadius: wp('3%'),
    borderTopRightRadius: wp('3%'),
    borderWidth: 1,
  },
});

const mapStateToProps = ({AuthReducer, network}) => {
  const {token, infoUser} = AuthReducer;
  const {isConnected} = network;
  return {isConnected, token, infoUser};
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({dataUser, isLogged, tokenUser}, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(ListHuruf);

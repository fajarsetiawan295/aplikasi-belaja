import React, {Component} from 'react';
import {View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native';
import {Icon, Header} from './../../component';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {colors} from '../../conf';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {dataUser, isLogged, tokenUser} from './../../redux/actions';

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  onUserLogout() {
    this.props.tokenUser('logout');
    this.props.isLogged(false);
    this.props.navigation.navigate('Login');
  }

  render() {
    const {navigation} = this.props;
    const List = ({onPress, title, IconLeft}) => {
      return (
        <TouchableOpacity style={styles.containerAddress} onPress={onPress}>
          <View style={styles.subContainerAddress}>
            {IconLeft}
            <Text style={styles.textList}>{title}</Text>
            <Icon.AntDesign name="rightcircle" color="#727C8E" size={24} />
          </View>
        </TouchableOpacity>
      );
    };

    return (
      <View style={styles.container}>
        <View style={styles.containerProfile}>
          <Image
            source={{
              uri:
                'https://png.pngtree.com/element_our/png_detail/20181102/avatar-profile-logo-vector-emblem-illustration-modern-illustration-png_227486.jpg',
            }}
            style={styles.imageProfile}
          />
        </View>
        <View
          style={{
            height: hp('40%'),
            width: wp('90%'),
            backgroundColor: 'white',
            padding: wp('4%'),
            justifyContent: 'center',
          }}>
          <Text style={{fontWeight: 'bold'}}>Informasi Profile</Text>
          <View
            style={{
              marginTop: hp('2%'),
              width: wp('80%'),
              backgroundColor: 'white',
              padding: wp('4%'),
              borderWidth: 1,
            }}>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <Text>Nama</Text>
              <Text>{this.props.infoUser.name}</Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginTop: hp('3%'),
              }}>
              <Text>Email</Text>
              <Text>{this.props.infoUser.email}</Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginTop: hp('3%'),
              }}>
              <Text>Sekolah</Text>
              <Text>{this.props.infoUser.sekolah}</Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginTop: hp('3%'),
              }}>
              <Text>Kelas</Text>
              <Text>{this.props.infoUser.kelas}</Text>
            </View>
          </View>
        </View>

        <TouchableOpacity
          onPress={() => this.onUserLogout()}
          style={styles.buttonLogOut}>
          <Text style={styles.textLogOut}>LOG OUT</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#E9EBE4',
  },
  containerProfile: {
    marginTop: hp('5'),
    width: wp('90'),
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    marginBottom: hp('5%'),
  },
  profileRight: {
    width: wp('55'),
  },
  imageProfile: {
    width: wp('25'),
    height: wp('25'),
    borderRadius: wp('20'),
    borderWidth: 1,
    borderColor: 'white',
  },
  textNameProfile: {
    // fontFamily: Fonts.type.emphasis,
    // fontSize: Fonts.size.regular,
  },
  textEmailProfile: {
    // fontSize: Fonts.size.small,
    // fontFamily: Fonts.type.emphasis,
    color: '#515C6F',
  },
  buttonEditProfile: {
    marginTop: hp('2'),
    borderRadius: 14,
    width: wp('50'),
    borderColor: '#727C8E4D',
    borderWidth: 1,
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textButtonEditProfile: {
    color: '#727C8E',
    marginVertical: wp('1'),
  },
  containerAddress: {
    marginVertical: hp('1'),
    width: wp('90'),
    backgroundColor: 'white',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: wp('1%'),
  },
  subContainerAddress: {
    marginVertical: hp('1'),
    justifyContent: 'space-between',
    width: wp('80'),
    alignItems: 'center',
    flexDirection: 'row',
  },
  textList: {
    width: wp('50'),
    color: '#515C6F',
  },
  buttonLogOut: {
    marginTop: wp('5%'),
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    width: wp('90%'),
    marginBottom: hp('5'),
    backgroundColor: 'white',
  },
  textLogOut: {
    marginVertical: hp('2%'),
    fontFamily: 'ProximaNovaSemiBold',
    color: '#1C1819',
  },
});

const mapStateToProps = ({AuthReducer, network}) => {
  const {token, infoUser} = AuthReducer;
  const {isConnected} = network;
  return {isConnected, token, infoUser};
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({dataUser, isLogged, tokenUser}, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(Profile);

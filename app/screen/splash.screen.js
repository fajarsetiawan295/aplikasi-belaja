import React, {Component} from 'react';
import {View, Text} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {isLogged, tokenfcm} from '../redux/actions';

class splashScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  UNSAFE_componentWillMount() {
    const {navigation} = this.props;
    if (this.props.isLogged) {
      navigation.navigate('Home');
    } else {
      navigation.navigate('Login');
    }
  }

  render() {
    return <View />;
  }
}

const mapStateToProps = ({AuthReducer}) => {
  const {isLogged} = AuthReducer;
  return {isLogged};
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({tokenfcm}, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(splashScreen);
